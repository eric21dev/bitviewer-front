# bitview-front
Contains Angular application.
Consumes API endpoint from Django application.
Visualizes a chart with min / max values (i used chart.js for this)
Each 1minute chart is updated.

# install front
cd bitviewer-front
npm install
npm start

# serve front builded
cd bitviewer-front/www
http-serve
Now front is running on port 8080