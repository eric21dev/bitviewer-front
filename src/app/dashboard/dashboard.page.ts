import { ApiService } from './../services/api.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  @ViewChild('barChart',  { read: ElementRef, static: false }) barChart: ElementRef;
  TIME_REFRESH = 1000 * 60; // ms to refresh
  data: any;

  constructor(private api: ApiService) { }

  ngOnInit() {
    this.getData();
    setInterval(() => { this.getData(); }, this.TIME_REFRESH);
  }

  getData() {
    this.api.getVirtualCurrencies().toPromise().then((r: any) => {
      this.data = r;
      this.data.results = r.results.reverse();
      this.setupChart();
    }).catch( (e: any) => {});
  }


  setupChart() {
    let barConfig = {
      type: 'bar',
      data: {
        labels: [],
        datasets: [{
          label: 'Low',
          backgroundColor: 'rgba(255, 99, 132, 0.2)',
          borderColor: 'rgba(255, 99, 132, 0.2)',
          data: [],
          fill: false,
        }, {
          label: 'High',
          fill: false,
          backgroundColor: 'rgba(54, 162, 235, 1)',
          borderColor: 'rgba(54, 162, 235, 1)',
          data: [],
        }]
      },
      options: {
        responsive: true,
        title: {
          display: true,
          text: this.data.results[0].symbol
        },
      }
    };
    this.data.results.forEach((element: any) => {
      const d = new Date(element.date_updated);
      const datestring = d.getDate()  + '/' + (d.getMonth() + 1) + '/' + d.getFullYear()  + ' ' + d.getHours() + ':' + d.getMinutes();
      barConfig.data.labels.push(datestring);
      barConfig.data.datasets[0].data.push(element.low);
      barConfig.data.datasets[1].data.push(element.high);
    });
    this.barChart = new Chart(this.barChart.nativeElement, barConfig);
  }

}




