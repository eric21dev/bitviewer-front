import {
    Injectable
} from '@angular/core';
import {
    HttpClient,
    HttpHeaders
} from '@angular/common/http';
import {
    map
} from 'rxjs/operators';
import {
    environment
} from '../../environments/environment';
@Injectable({
    providedIn: 'root'
})
export class ApiService {

    constructor(private httpClient: HttpClient) {}

    private getOptions() {
        return {
            headers: new HttpHeaders({
                Accept: 'application/json',
                'content-type': 'application/json',
            })
        };
    }

    getVirtualCurrencies() {
        return this.httpClient.get(`${environment.apiURL}/api/v1/virtualCurrencies/`, this.getOptions()).pipe(map(r => r));
    }

}
